<div class="w-full px-3 pb-6 lg:max-w-6xl lg:mx-auto">
    <div class="mb-20">
        <div class="text-3xl text-gray-900 pb-12 flex justify-center">
            <span class="ml-3 font-bold"></span>
        </div>
        <div class="grid gap-6 grid-cols-2">
            <a href=""
               class="block p-5 rounded-lg bg-white hover:shadow-lg course-item group">
                <div class="flex">
                    <div>
                        <img src="" class="rounded-lg object-cover" width="133" height="100">
                    </div>
                    <div class="ml-5">
                        <div class="text-xl text-gray-800 font-bold"></div>
                        <div class="mt-8 text-xs">
                            <span class="py-1 px-2 rounded-lg bg-red-100 text-red-500"></span>
                            <span class="ml-5 text-gray-600"></span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>